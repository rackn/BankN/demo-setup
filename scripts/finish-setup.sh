#!/usr/bin/env bash

PREFIX=$1
DRP_VERSION=$2
DEFAULT_BROKER=$3

echo "Completing setup: $1"

result=$(drpcli machines await Name:bankn-manager-1 'Or(Runnable=Eq(false),WorkflowComplete=Eq(true))' --timeout 360)
if [[ "$result" != "complete" ]] ; then
    echo "Failed complete bootstrap."
    echo "Finish bootstrapping and run:"
    echo "  $0 $PREFIX $DRP_VERSION $DEFAULT_BROKER"
    exit 1
fi

if [[ ! -f rackn-license.json ]] ; then
    echo
    echo "No license file provided, please update after by:"
    echo "  * Import license file and check/update to generate valid license in the UX"
    echo "  * Create a new license in the UX."
    echo
    echo "Download the license from the UX and place in this directory as rackn-license.json"
    echo "  * drpcli contents show rackn-license > rackn-license.json"
    echo
    echo "Finish bootstrapping and run:"
    echo "  $0 $PREFIX $DRP_VERSION $DEFAULT_BROKER"
    exit 1
fi

echo "Add cloud profiles"
curl -o cloud-profiles.sh https://gitlab.com/rackn/provision-content/-/raw/v4/tools/cloud-profiles.sh
chmod +x cloud-profiles.sh
./cloud-profiles.sh
rm -f cloud-profile.sh

echo "Load demo-content"
cd demo-setup
git pull || :
rm -rf rebar-catalog
tools/build_content.sh
find rebar-catalog -type f | while read f ; do
    drpcli files upload $f as $f >/dev/null
    drpcli contents upload $f >/dev/null
done
cd ..

# Trigger demo-setup
cat > event.yaml <<EOF
---
Type: bankn-demo-setup
Action: start
Key: $PREFIX
Object:
  demo-setup/prefix: $PREFIX
  demo-setup/drp-version: $DRP_VERSION
  demo-setup/default-broker: $DEFAULT_BROKER
EOF
drpcli events post event.yaml
rm -f event.yaml

echo "Watch for the system complete building other nodes before moving onto the next part of the Demo"
echo "Demo setup is complete."