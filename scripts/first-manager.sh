#!/usr/bin/env bash

set -e

# BUMP version on updates
INSTALL_VERSION="v22.02.22-2"

DEFAULT_DRP_VERSION=${DEFAULT_DRP_VERSION:-"tip"}
DEFAULT_BROKER=${DEFAULT_BROKER:-"linode-broker"}
DEMO_SETUP="https://gitlab.com/rackn/BankN/demo-setup.git"

exit_cleanup() {
  local _x=$1
  shift
  rm -f rackn-catalog.json
  [[ -n "$*" ]] && echo -e "EXIT MESSAGE: $*"
  exit $_x
}

COLOR_OK=true
set_color() {
# terminal colors
RCol='\e[0m'    # Text Reset

# Regular           Bold                Underline           High Intensity      BoldHigh Intens     Background          High Intensity Backgrounds
Bla='\e[0;30m';     BBla='\e[1;30m';    UBla='\e[4;30m';    IBla='\e[0;90m';    BIBla='\e[1;90m';   On_Bla='\e[40m';    On_IBla='\e[0;100m';
Red='\e[0;31m';     BRed='\e[1;31m';    URed='\e[4;31m';    IRed='\e[0;91m';    BIRed='\e[1;91m';   On_Red='\e[41m';    On_IRed='\e[0;101m';
Gre='\e[0;32m';     BGre='\e[1;32m';    UGre='\e[4;32m';    IGre='\e[0;92m';    BIGre='\e[1;92m';   On_Gre='\e[42m';    On_IGre='\e[0;102m';
Yel='\e[0;33m';     BYel='\e[1;33m';    UYel='\e[4;33m';    IYel='\e[0;93m';    BIYel='\e[1;93m';   On_Yel='\e[43m';    On_IYel='\e[0;103m';
Blu='\e[0;34m';     BBlu='\e[1;34m';    UBlu='\e[4;34m';    IBlu='\e[0;94m';    BIBlu='\e[1;94m';   On_Blu='\e[44m';    On_IBlu='\e[0;104m';
Pur='\e[0;35m';     BPur='\e[1;35m';    UPur='\e[4;35m';    IPur='\e[0;95m';    BIPur='\e[1;95m';   On_Pur='\e[45m';    On_IPur='\e[0;105m';
Cya='\e[0;36m';     BCya='\e[1;36m';    UCya='\e[4;36m';    ICya='\e[0;96m';    BICya='\e[1;96m';   On_Cya='\e[46m';    On_ICya='\e[0;106m';
Whi='\e[0;37m';     BWhi='\e[1;37m';    UWhi='\e[4;37m';    IWhi='\e[0;97m';    BIWhi='\e[1;97m';   On_Whi='\e[47m';    On_IWhi='\e[0;107m';

# palette
CWarn="$BYel"
CFlag="$IBlu"
CDef="$IBla"
CFile="$IBla"
CNote="$Yel"
CInfo="$Cya"
CNotice="$IYel"
COk="$IGre"
CErr="$IRed"
}

c_def() { echo -en "$CDef$@$RCol";}
c_flag() { echo -en "$CFlag$@$RCol";}
c_warn() { echo -en "$CWarn$@$RCol";}
c_file() { echo -en "$CFile$@$RCol";}
c_err() { echo -en "$CErr$@$RCol";}

usage() {
    [[ "$COLOR_OK" == "true" ]] && set_color
echo -e "
${ICya}USAGE:${RCol} ${BYel}$0${RCol} $(c_flag "[ <prefix> ] [ <argument flags: see below> ]")


${ICya}OPTIONS${RCol}:
    $(c_flag "<prefix>")                - The prefix to add to all created machines

${ICya}ARGUMENT FLAGS${RCol}:
    $(c_flag "--debug")=$(c_def "[true|false]")    - Enables debug output
    $(c_flag "--no-sudo")               - Do not use \"sudo\" prefix on commands (assume you're root)
    $(c_flag "--no-colors")             - Installer output will have no terminal colors
    $(c_flag "--demo-setup")=$(c_def "\"<url>\"")
                            - Define repo url for the demo-setup repo
                              The default is ${DEMO_SETUP}.
    $(c_flag "--default-broker")=$(c_def "\"<string>\"")
                            - Define the default broker for the system create resources in.
                              The default is ${DEFAULT_BROKER}.

${ICya}DEFAULTS${RCol}:
    |  argument flag:        def. value:      |  argument flag:        def. value:
    |  -------------------   ------------     |  ------------------    ------------
    |  debug               = $(c_def "false")            |  demo-setup          = $(c_def "$DEMO_SETUP")
    |  version (*)         = $(c_def "$DEFAULT_DRP_VERSION")              |  default-broker      = $(c_def "$DEFAULT_BROKER")

    * version examples: '$(c_def "tip")', '$(c_def "v4.6.3")', '$(c_def "v4.7.0-beta1.3")', or '$(c_def "stable")'

${ICya}PREREQUISITES${RCol}:
    ${CNote}NOTE: By default, prerequisite packages will be installed if possible.  You must
          ${CNote}manually install these first on a Mac OS X system. Package names may vary
          ${CNote}depending on your operating system version/distro packaging naming scheme.${RCol}

    ${ICya}REQUIRED${RCol}: curl, git, jq

${ICya}INSTALLER VERSION${RCol}:  $(c_def "$INSTALL_VERSION")
"
} # end usage()

# control flags
DBG=false
_sudo="sudo"

original_args=( "$@" )
args=()
while (( $# > 0 )); do
    arg="$1"
    arg_key="${arg%%=*}"
    arg_data="${arg#*=}"
    case $arg_key in
        --help|-h)                  usage; exit_cleanup 0                             ;;
        --debug)                    DBG=true;                                         ;;
        --version|--drp-version)    DRP_VERSION=${arg_data}                           ;;
        --demo-setup)               DEMO_SETUP="${arg_data}"                          ;;
        --default-broker)           DEFAULT_BROKER="${arg_data}"                      ;;
        --no-sudo)                  _sudo=""                                          ;;
        --no-colors)                COLOR_OK=false                                    ;;
        --*)
            arg_key="${arg_key#--}"
            arg_key="${arg_key//-/_}"
            # "^^" Paremeter Expansion is a bash v4.x feature; Mac by default is bash 3.x
            #arg_key="${arg_key^^}"
            arg_key=$(echo $arg_key | tr '[:lower:]' '[:upper:]')
            echo -e "$PREF_INFO Overriding $arg_key with $arg_data"
            export $arg_key="$arg_data"
            ;;
        *)
            args+=("$arg");;
    esac
    shift
done

[[ "$COLOR_OK" == "true" ]] && set_color
PREF_OK="$COk>>>$RCol"
PREF_ERR="$CErr!!!$RCol"
PREF_INFO="$CInfo###$RCol"
PREF_WARN="${CWarn}Warning$RCol:"

set -- "${args[@]}"

if [[ "$HA_ENABLED" == "true" ]] ; then
  if [[ "$HA_TOKEN" == "" ]] ; then
    if [[ "$HA_PASSIVE" == "true" ]] ; then
      usage; exit_cleanup 1 "Passive systems must have a token. Specify HA_TOKEN."
    fi
    HA_TOKEN="ACTIVE_NO_TOKEN"
  fi
fi

DRP_VERSION=${DRP_VERSION:-"$DEFAULT_DRP_VERSION"}

[[ $DBG == true ]] && set -x

if [[ $EUID -eq 0 ]]; then
    _sudo=""
else
    if [[ ! -x "$(command -v sudo)" ]]; then
        exit_cleanup 1 "$(c_err "FATAL"): Script is not running as root and sudo command is not found. Please be root"
    fi
fi

# Figure out what Linux distro we are running on.
export OS_TYPE= OS_VER= OS_NAME= OS_FAMILY=

if [[ -f /etc/os-release ]]; then
    . /etc/os-release
    OS_TYPE=${ID,,}
    OS_VER=${VERSION_ID,,}
elif [[ -f /etc/lsb-release ]]; then
    . /etc/lsb-release
    OS_VER=${DISTRIB_RELEASE,,}
    OS_TYPE=${DISTRIB_ID,,}
elif [[ -f /etc/centos-release || -f /etc/fedora-release || -f /etc/redhat-release ]]; then
    for rel in centos-release fedora-release redhat-release; do
        [[ -f /etc/$rel ]] || continue
        OS_TYPE=${rel%%-*}
        OS_VER="$(egrep -o '[0-9.]+' "/etc/$rel")"
        break
    done

    if [[ ! $OS_TYPE ]]; then
        exit_cleanup 1 "$(c_err "FATAL"): Cannot determine Linux version we are running on!"
    fi
elif [[ -f /etc/debian_version ]]; then
    OS_TYPE=debian
    OS_VER=$(cat /etc/debian_version)
elif [[ $(uname -s) == Darwin ]] ; then
    OS_TYPE=darwin
    OS_VER=$(sw_vers | grep ProductVersion | awk '{ print $2 }')
fi
OS_NAME="$OS_TYPE-$OS_VER"

case $OS_TYPE in
    amzn|centos|redhat|fedora) OS_FAMILY="rhel"      ;;
    debian|ubuntu|raspbian)        OS_FAMILY="debian"    ;;
    coreos)               OS_FAMILY="container" ;;
    *)                    OS_FAMILY="$OS_TYPE"  ;;
esac

# install the EPEL repo if appropriate, and not enabled already
install_epel() {
    if [[ $OS_FAMILY == rhel ]] ; then
        if ( `yum repolist enabled | grep -q "^epel/"` ); then
            echo -e "$PREF_INFO EPEL repository installed already."
        else
            if [[ $OS_TYPE != fedora ]] ; then
                $_sudo yum install -y epel-release
            fi
        fi
    fi
}

check_bins_darwin() {
  local _bin=$1
     case $_bin in
        *)
            if ! which $_bin > /dev/null 2>&1; then
                echo -e "$PREF_ERR Must have binary '$_bin' installed."
                echo "eg:   brew install $_bin"
                error 1
            fi
         ;;
     esac
} # end check_bins_darwin()

# handle RHEL and Debian types
check_pkgs_linux() {
    # assumes binary and package name are same
    local _pkg=$1
        if ! which $_pkg &>/dev/null; then
            echo -e "$PREF_INFO Missing dependency '$_pkg', attempting to install it... "
            if [[ $OS_FAMILY == rhel ]] ; then
                echo $IN_EPEL | grep -q $_pkg && install_epel
                $_sudo yum install -y $_pkg
            elif [[ $OS_FAMILY == debian ]] ; then
                $_sudo apt-get install -y $_pkg
            fi
        fi
} # end check_pkgs_linux()

ensure_packages() {
    echo -e "$PREF_OK Ensuring required tools are installed"
    case $OS_FAMILY in
        darwin)
            error=0
            BINS="curl jq git"
            for BIN in $BINS; do
                check_bins_darwin $BIN
            done

            if [[ $error == 1 ]]; then
                echo -e "$PREF_ERR After install missing components, restart the terminal to pick"
                echo "  up the newly installed commands, and re-run the installer."
                echo
                exit_cleanup 1
            fi
        ;;
        rhel|debian)
            PKGS="curl jq git"
            IN_EPEL="curl"
            for PKG in $PKGS; do
                check_pkgs_linux $PKG
            done
        ;;
        coreos)
            echo -e "$PREF_INFO CoreOS does not require any packages to be installed.  DRP will be"
            echo "  installed from the Docker Hub registry."
        ;;
        photon)
            if ! which tar > /dev/null 2>&1; then
                echo -e "$PREF_OK Installing packages for Photon Linux..."
                tdnf -y makecache
                tdnf -y install git jq
            else
                echo -e "$PREF_INFO 'tar' already installed on Photon Linux..."
            fi
        ;;
        *)
            exit_cleanup 1 "$(c_err "FATAL"): Unsupported OS Family ($OS_FAMILY)."
        ;;
    esac
} # end ensure_packages()

PREFIX=$1

ensure_packages

rm -rf demo-setup
git clone ${DEMO_SETUP}

if which firewall-cmd >/dev/null 2>/dev/null ; then
   echo "Updating firewall ports"
   firewall-cmd --permanent --add-port=8090/tcp
   firewall-cmd --permanent --add-port=8091/tcp
   firewall-cmd --permanent --add-port=8092/tcp
   firewall-cmd --reload
fi

if [[ -f rackn-license.json ]] ; then
    echo "Updating license"

    RSI_LICENSE_KEY=$(cat rackn-license.json | jq -r '.sections.profiles["rackn-license"].Params["rackn/license"]')
    RSI_DRP_ID=bankn-manager-1
    code=$(curl -o rackn-new-license.json -w "%{http_code}" -X POST -H "rackn-endpointid: $RSI_DRP_ID" -H "Authorization: $RSI_LICENSE_KEY" https://cloudia.rackn.io/api/v1/license/update)
    if [[ "$code" == "304" ]] ; then
        echo "License is good"
    elif [[ "$code" == "200" ]] ; then
        echo "license updated"
        mv rackn-new-license.json rackn-license.json
    else
        echo "Failed to update license"
    fi
    rm -f rackn-new-license.json
    RSI_LICENSE_ARGS="--license-file=rackn-license.json"
fi

curl -fsSL get.rebar.digital/${DRP_VERSION} | bash -s install --universal --initial-profiles=bootstrap-manager --initial-plugins=triggers --version=${DRP_VERSION} --drp-id=bankn-manager-1 --ha-id=bankn-manager $RSI_LICENSE_ARGS

demo-setup/scripts/finish-setup.sh $PREFIX $DRP_VERSION $DEFAULT_BROKER
