
demo-setup
----------

Tools for setting up the BankN demo.

Use these tools and the docs to setup the demo.

Requirements
============

* Create a linode or AWS system to act as the main manager.

  * CentOS Stream 8
  * Dedicated 4GB
  * Make sure curl is installed

Staging
=======

Before you start, review these items:

* If you have a license file, place it in the root directory as rackn-license.json.

  The license will be updated to include the endpoint, `bankn-manager-1`.  This will be the top-level
  manager for this environment.

* Configure cloud credentials for system to operate

  * LINODE - will use the credentials in ~/.linode/credentials

    This file just has the token as a single-ling string.

    .. code::

      MY_TOKEN_HERE

  * AWS - will use the credentials in ~/.aws/credentials

    This is an ini-like file.  Something like this will be processed.  Only one credential set is expected in the
    file.

    .. code::

      [default]
      aws_access_key_id = MY_ACCESS_KEY_ID
      aws_secret_access_key = MY_ACCESS_KEY

  * Digital Ocean - will use the credentials in ~/.digitalocean/credentials

    This file just has the token as a single-ling string.

    .. code::

      MY_TOKEN_HERE

  * Oracle Cloud - will use the credentials in ~/.oci/config

    This file is a key value definition in the format key=value.

    .. code::

      key_file=path_to_public_key_file
      user=my_username
      tenancy=my_tenancy_group
      fingerprint=my_fingerprint
      region=my_region

  * Google Cloud - will use the credentials in `~/.gconf/desktop/\*.json`

    This will choose the first file that matches `~/.gconf/desktop/\*.json`.
    The whole json blob will be added to the broker accessing Google Cloud.

  * PNAP - will use the credentials in ~/.pnap/config.yaml

    This file is a key value definition in the format of a top-level object.

    .. code::

      ---
      clientId: client_id
      clientSecret: my_client_secret

  * Azure - Assumes that the `az` tool is installed and already logged in.

    This will assume that the `az` tool has been logged in and can query the current user information.

    .. error::

      THIS MAY NOT WORK DURING THE AUTOMATION.  NEED TO TEST. XXX:


Setup
=====

To get started, run (change `mydemo` to your string prefix, no spaces):

.. code::

  curl -fsSL https://gitlab.com/rackn/BankN/demo-setup/-/raw/v4/scripts/first-manager.sh | bash -s mydemo


Additional arguments can be added to the command line.

.. code::

  USAGE: scripts/first-manager.sh [ <prefix> ] [ <argument flags: see below> ]

  OPTIONS:
    <prefix>                - The prefix to add to all created machines

  ARGUMENT FLAGS:
    --debug=[true|false]    - Enables debug output
    --no-sudo               - Do not use "sudo" prefix on commands (assume you're root)
    --no-colors             - Installer output will have no terminal colors
    --demo-setup="<url>"
                            - Define repo url for the demo-setup repo
                              The default is https://gitlab.com/rackn/BankN/demo-setup.git.
    --default-broker="<string>"
                            - Define the default broker for the system create resources in.
                              The default is linode-broker.

  DEFAULTS:
    |  argument flag:        def. value:      |  argument flag:        def. value:
    |  -------------------   ------------     |  ------------------    ------------
    |  debug               = false            |  demo-setup          = https://gitlab.com/rackn/BankN/demo-setup.git
    |  version (*)         = tip              |  default-broker      = linode-broker

    * version examples: 'tip', 'v4.6.3', 'v4.7.0-beta1.3', or 'stable'

  PREREQUISITES:
    NOTE: By default, prerequisite packages will be installed if possible.  You must
          manually install these first on a Mac OS X system. Package names may vary
          depending on your operating system version/distro packaging naming scheme.

    REQUIRED: curl, git, jq

  INSTALLER VERSION:  v22.02.22-2

The default is to use the `linode-broker` and `tip` release.

What Happens?
=============

The `first-manager.sh` script installs DRP in the current machine at the specified version.  The DRP
server will have an endpoint id of `bankn-manager-1` and high-availability id or `bankn-manager`.

The default admin user and password will be `rocketskates` and `r0cketsk8tes`.  The UX can be accessed
by https://<IP>:8092 where *IP* is the IP address of the endpoint.

The DRP endpoint will bootstrap into running as a manager downloading and setup up an initial catalog.

Once the server is bootstrapped, a work order is started on the server to do the rest of the setup.  This
is part of the blueprint, `bankn-demo-setup`.

The following event injected into the system start the whole process.

.. code::

  ---
  Type: bankn-demo-setup
  Action: start
  Key: $PREFIX
  Object:
    demo-setup/prefix: $PREFIX
    demo-setup/drp-version: $DRP_VERSION
    demo-setup/default-broker: $DEFAULT_BROKER


